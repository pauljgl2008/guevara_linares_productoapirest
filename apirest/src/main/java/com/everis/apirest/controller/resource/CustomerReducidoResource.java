package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class CustomerReducidoResource {
	private String nombre;
	private String apellidoCompleto;
}