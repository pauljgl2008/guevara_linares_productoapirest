package com.everis.apirest.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.CustomerReducidoResource;
import com.everis.apirest.controller.resource.CustomerResource;
import com.everis.apirest.controller.resource.ProductoIGVResource;
import com.everis.apirest.controller.resource.ProductoReducidoResource;
//import com.everis.apirest.controller.resource.InfoResource;
//import com.everis.apirest.controller.resource.NameResource;
import com.everis.apirest.controller.resource.ProductoResource;
import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.model.entity.TipoProducto;
import com.everis.apirest.service.CustomerService;
import com.everis.apirest.service.ProductoService;


@RestController
public class ApiController {
	
	@Autowired
	ProductoService productoService;
	@Autowired
	CustomerService customerService;

	//@Autowired
	//private ProductoRepository productoRepository; 
	
	@Value("${igv}")
	BigDecimal igv;
	
	@GetMapping("/igv")
	public BigDecimal igv(){
		return igv;
	}
	
	@PostMapping("/producto")
	public ProductoResource guardarProducto(@RequestBody ProductoReducidoResource request) throws Exception {
		
		TipoProducto tipoProducto = productoService.obtenerTipoProducto(request.getCodigoTipoProducto());
		
		Producto nuevo = new Producto();
		nuevo.setNombre(request.getNombre());
		nuevo.setCodigo(request.getCodigo());
		nuevo.setDescripcion(request.getDescripcion());
		nuevo.setPrecio(request.getPrecio());
		nuevo.setTipoProducto(tipoProducto);
		Producto producto = productoService.insertar(nuevo);
		
		ProductoResource productoResource = new ProductoResource();
		productoResource.setId(producto.getId());
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setCodigo(producto.getCodigo());
		productoResource.setPrecio(producto.getPrecio());
		productoResource.setTipoProducto(producto.getTipoProducto().getCodigo());
		productoResource.setActivo(producto.getActivo());

		return productoResource;
 
	}
	@GetMapping("/productos")
	public List<ProductoResource> obtener2(){
		
		List<ProductoResource> listado = new ArrayList<>();
		
		productoService.obtenerProductos().forEach(producto -> {
			ProductoResource productoResource = new ProductoResource();
			productoResource.setId(producto.getId());
			productoResource.setNombre(producto.getNombre());
			productoResource.setCodigo(producto.getCodigo());
			productoResource.setDescripcion(producto.getDescripcion());
			productoResource.setPrecio(producto.getPrecio());
			productoResource.setTipoProducto(producto.getTipoProducto().getNombre());
			productoResource.setActivo(producto.getActivo());
			listado.add(productoResource);
		});
		
		return listado;
		/*productoRepository.findAll().forEach(producto -> {
			ProductoResource productoResource = new ProductoResource();
			productoResource.setId(producto.getId());
			productoResource.setNombre(producto.getName());
			productoResource.setDescripcion(producto.getDescription());
			listado.add(productoResource);
		});
		
		return listado;*/
				
	}
	
	@GetMapping("/productos/{id}")
	public ProductoIGVResource obtenerProductoPorId(@PathVariable("id") Long id) throws Exception {
		Producto producto = productoService.obtenerProductoPorId(id);
		ProductoIGVResource productoIGVResource = new ProductoIGVResource();
		productoIGVResource.setId(producto.getId());
		productoIGVResource.setNombre(producto.getNombre());
		productoIGVResource.setCodigo(producto.getCodigo());
		productoIGVResource.setPrecio(producto.getPrecio());
		BigDecimal precioNeto = (producto.getPrecio().multiply(igv)).add(producto.getPrecio());
		productoIGVResource.setPrecioNeto(precioNeto);

		return productoIGVResource;
	}
	
	@GetMapping("/customers")
	public List<CustomerResource> obtenerCustomers(){
		
		List<CustomerResource> listado = new ArrayList<>();
		
		customerService.obtenerCustomers().forEach(customer -> {
			CustomerResource customerResource = new CustomerResource();
			customerResource.setId(customer.getId());
			customerResource.setNombre(customer.getName());
			customerResource.setApellidoCompleto(customer.getLastName()+" "+ customer.getLastName2());
			listado.add(customerResource);
		});
		
		return listado;
		/*productoRepository.findAll().forEach(producto -> {
			ProductoResource productoResource = new ProductoResource();
			productoResource.setId(producto.getId());
			productoResource.setNombre(producto.getName());
			productoResource.setDescripcion(producto.getDescription());
			listado.add(productoResource);
		});
		
		return listado;*/
				
	}
	
	@PostMapping("/customers")
	public CustomerResource guardarCliente(@RequestBody CustomerReducidoResource request) {
		Customer customer = customerService.guardar(request);
		
		CustomerResource customerResource = new CustomerResource();
		customerResource.setId(customer.getId());
		customerResource.setNombre(customer.getName());
		
		String apellidos = customer.getLastName() + " " + customer.getLastName2();
		
		if(customer.getLastName2() == null) {
			apellidos = customer.getLastName();
		}
		
		customerResource.setApellidoCompleto(apellidos);
	 
		return customerResource;
 
	}

	
	/*@GetMapping("/productos")
	public Iterable<Producto> obtener(){
		return productoRepository.findAll();
	}*/
	
	/*@GetMapping("/obtener")
	public NameResource obtenerNombre() {
		NameResource obj = new NameResource();
		obj.setName("java apirest");
		obj.setDescripcion("Virtual machine");
		return  obj;
	}
	@GetMapping("/info")
	public InfoResource info() {
		InfoResource obj = new InfoResource();
		obj.setNombre("Paúl Guevara");
		obj.setUsuario("pguevarl");
		return  obj;
	}*/
}
