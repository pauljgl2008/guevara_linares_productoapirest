package com.everis.apirest.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data

public class ProductoReducidoResource {
	private String nombre;
	private String codigo;
	private String descripcion;
	private BigDecimal precio;
	private String codigoTipoProducto;// -> primero buscar el código del tipo de producto y almacenarlo
}
