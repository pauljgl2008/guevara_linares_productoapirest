package com.everis.apirest.controller.resource;

import lombok.Data;

@Data

public class TipoProductoResource {
	private Long id;
	private String nombre;
	private String codigo;
}
