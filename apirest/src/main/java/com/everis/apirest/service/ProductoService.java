package com.everis.apirest.service;

import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.model.entity.TipoProducto;

public interface  ProductoService {
		
	public Iterable<Producto> obtenerProductos();
	
	public Producto insertar(Producto producto) throws Exception;;
	
	public Producto obtenerProductoPorId(Long id) throws Exception;
	public TipoProducto  obtenerTipoProducto(String codigoTipoProducto) throws Exception;

	
}
