package com.everis.apirest.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.apirest.model.entity.Producto;

@Repository
public interface ProductoRepository extends CrudRepository<Producto, Long> {

}
